package uvsq.ProjetGL;

import java.sql.Connection;  
import java.sql.DriverManager;

import java.sql.SQLException;
import java.sql.Statement;

public class Db_init {

public static void dbinit() throws SQLException, ClassNotFoundException {
		
		//on charge le driver 		
		//dire que mysql c notre SGBD et annuaire notre BD : chaine de connection!
		String dburl = "jdbc:mysql://localhost/";
		String user = "root";
		String password ="";
       
		/*
				Properties connectionProps = new Properties( ) ;//connecter à un SGBD
				connectionProps.put ( "user" , userName ) ;
				connectionProps.put ( " password " , password ) ;
		*/
		
		//la requete en version chaine de caractere 
		String creaf = "CREATE TABLE IF NOT EXISTS `fichier` "
				    + "(`id` int(10) unsigned NOT NULL AUTO_INCREMENT,"
				    + " `nom` varchar(100) DEFAULT NULL, "
				    + "  `taille` int(10) unsigned NOT NULL, "
				    + " `format` varchar(100) DEFAULT NULL, "
				    + " `annotation` varchar(100) DEFAULT NULL,"
				    + " `contenu` varchar(1000) DEFAULT NULL,"
				    
				    + " PRIMARY KEY (`id`));";
		
		String creaD = "CREATE TABLE IF NOT EXISTS `dossier`"
			       + "(`id` int(10) unsigned NOT NULL AUTO_INCREMENT,"
			       + " `nom` varchar(100) DEFAULT NULL, "
			       + " `chemin` varchar(100) DEFAULT NULL,"
			       + "`idroot` int(10) unsigned, "
			       + "PRIMARY KEY (`id`), "  ///eviter d'avoir deux dossier au même nom dans un dossier 
			       + "FOREIGN KEY (idroot) REFERENCES dossier(id));";
		
	   String creafD = "CREATE TABLE IF NOT EXISTS `fich_dos`"
			       + "(`idfichier` int(10) unsigned,"
			       + " `iddossier` int(10) unsigned, "
			       + "PRIMARY KEY (`idfichier`,`iddossier`), "
			       + "FOREIGN KEY (idfichier) REFERENCES fichier(id), "
			       + "FOREIGN KEY (iddossier) REFERENCES dossier(id));";
		
	  
		  
	   // insertion dossier root de l'application qui est référencé à null
		String insert = "INSERT INTO dossier (nom, idroot,chemin) VALUES ('/', null,'/');";
					
		// preparedStatement = connection.prepareStatement("");
		//JDBC fait appel à notre driver en utilisant DriverManager, on connecte avec le SGBD
		try ( Connection conn = (Connection) DriverManager.getConnection(dburl,user,password) ) {
			
			//c'est objetc qui va envoyer mes requetes chez le SGBD
			// l'interface statement représente une instruction sql
			Statement s	= conn.createStatement();
			
			//s envoie la requete crea pour que le SGBD l'execute 
			s.executeUpdate("DROP DATABASE IF EXISTS `projetgl`;");
			s.executeUpdate("CREATE DATABASE `projetgl`;");
			s.executeUpdate("USE `projetgl`;");			
			
			s.executeUpdate("DROP TABLE IF EXISTS `dossier`;");
			s.executeUpdate(creaD);
			s.executeUpdate(insert);
			
			s.executeUpdate("DROP TABLE IF EXISTS `fichier`;");
			s.executeUpdate(creaf);
			s.executeUpdate("DROP TABLE IF EXISTS `fich_dos`;");
			s.executeUpdate(creafD);
			
		}
		catch(SQLException e) {
			if(e.getSQLState().equals("42Y55")) {//on peut pas drop si table n'existe pas : code erreur 42Y55
				try ( Connection conn = DriverManager.getConnection (dburl)) {
					Statement s	= conn.createStatement();
					s.executeUpdate(creaf);
				//	s.executeUpdate(insert);
				}
			}
			else {
				e.printStackTrace();
			}
		}
	}

public static void main(String args[]) throws ClassNotFoundException, SQLException{
	Db_init.dbinit();
	
}
}
