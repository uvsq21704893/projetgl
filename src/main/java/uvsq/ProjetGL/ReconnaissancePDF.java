package uvsq.ProjetGL;

import java.io.BufferedWriter; 
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;


public class ReconnaissancePDF extends Reconnaissance {
	
	public File extraireText(File file) {
		PDDocument pd = null;
		BufferedWriter wr = null;
		File input = null; 
		File output = null;
		
		try {
			input = file;  // The PDF file from where you would like to extract
			output = new File("TextOfFileProcessed.txt"); // The text file where you are going to store the extracted data
			
			pd = PDDocument.load(input); 
			
			
				PDFTextStripper stripper = new PDFTextStripper();
				stripper.setStartPage(1); //Start extracting from first page till the end
				contenu = stripper.getText(pd);
				wr = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(output))); //pattern decorator
				stripper.writeText(pd, wr);
			
				//////////////// récupération des informations à partir de l'OCR
				
				if (contenu.length() <  100)
						{
					
					ReconnaissanceOCR ocr =new ReconnaissanceOCR();
							 ocr.extraireText(file);
							 contenu = ocr.contenu;
						wr = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(output))); //pattern decorator
							stripper.writeText(pd, wr);
						}
			

			// I use close() to flush the stream.
			if (pd != null) {
				pd.close();
			}
			wr.close();
		} 
		catch (Exception e){
			e.printStackTrace();
		}
		
		return output; 
	}
}
