package uvsq.ProjetGL;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Dossier_Dao implements DAO<Dossier> {

	Connection connection = Dossiercourant_singleton.getInstance().connection;

	@Override
	public Dossier create(Dossier obj) {
		try {

			String query = "INSERT INTO `dossier` (nom , idroot,chemin) VALUES " + "(\"" + obj.getNom() + "\" ,"
					+ obj.getDossierparent()+ " ,\""
					 + obj.getchemin()+ "\");";

			Statement statement = connection.createStatement();
			statement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
			ResultSet results = statement.getGeneratedKeys();
			results.next();
			obj.setId(results.getInt(1));
			statement.close();

			return obj;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Dossier find(int id) {
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM dossier where id=" + id + ";");

			Dossier dossier = null;

			while (resultSet.next()) {
				String nom = resultSet.getString("nom");
				int dossierparent = resultSet.getInt("idroot");
				String chemin= resultSet.getString("chemin");
				dossier = new Dossier(id, nom, dossierparent,chemin);
				resultSet.close();
				statement.close();

				return dossier;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Dossier find_nom(String nom, int idroot) { //// on peut penser à
														//// chercher un dossier
														//// avec le nom
														//// facture/pdf
		try {
			Statement statement = (Statement) connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("SELECT * FROM dossier where nom=\"" + nom + " \" AND idroot=" + idroot + ";");

			Dossier dossier = null;

			if(resultSet.next())
			{
			int id = resultSet.getInt("id");
			String chemin= resultSet.getString("chemin");
			dossier = new Dossier(id, nom, idroot,chemin);
			}
			resultSet.close();
			statement.close();
			return dossier;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Dossier> find_all(int idroot) { 
		try {
			Statement statement = (Statement) connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM dossier where idroot=" + idroot + ";");

			Dossier dossier = null;
			   ArrayList<Dossier>  dir = new ArrayList<Dossier>();
			while (resultSet.next()) {

				int id = resultSet.getInt("id");
				String nom = resultSet.getString("nom");
				String chemin= resultSet.getString("chemin");
				dossier = new Dossier(id, nom, idroot,chemin);
				dir.add(dossier);  
			}
			resultSet.close();
			statement.close();
			return dir;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public Dossier find_chemin(String chemin) { 
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM dossier where chemin=\"" + chemin + "\";");

			Dossier dossier = null;
		
				if(resultSet.next())
				{
				int id = resultSet.getInt("id");
				String nom = resultSet.getString("nom");
				int idroot= resultSet.getInt("idroot");
				dossier = new Dossier(id, nom, idroot,chemin);		
				}
				resultSet.close();
			statement.close();
			return dossier;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void delete(int id) {  /// non implémenté 
		try {
			Statement statement = connection.createStatement();
			Statement statement2 = connection.createStatement();
			 
			 ResultSet resultSet = statement.executeQuery("select * FROM fich_dos where iddossier=" + id + ";");
		
				while (resultSet.next()) {		
					Fichier_Dao bdd = new Fichier_Dao();
					bdd.delete(resultSet.getInt("idfichier"));
				}
				
			resultSet = statement.executeQuery("SELECT * FROM dossier where idroot=" + id + ";");
			if (resultSet.next())
			{
				delete(resultSet.getInt("id"));
				
			}		
			statement.executeUpdate("DELETE FROM dossier where id=" + id + ";");
		    
			resultSet.close();
			statement.close();
			statement2.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public Dossier update(String oldname, String newname) {
		// TODO Auto-generated method stub
		return null;
	}

}
