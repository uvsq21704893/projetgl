package uvsq.ProjetGL;

public class cmd_cd implements Commande {

	String nom;

	public cmd_cd(String nom) {
		this.nom = nom;
	}

	@Override
	public void executer() {
		String[] names = nom.split("/");
		String chemin="/";
		Dossier_Dao bdd = new Dossier_Dao();
		int i=0, id=1;		
		
		if(names.length>0)
		{

			
		if (names[0].length() == 0) {
			
			chemin = "";
			id = 1;
			for (i = 1; i < names.length; i++) {
				if (!names[i].equals("") && !names[i].equals("..") && !names[i].equals(".")) {
					chemin += "/" + names[i];
					
					Dossier dossier = bdd.find_chemin(chemin);
					if (dossier != null) {
						id = dossier.getId();
					} else {
						break;
					}

				}
				if (names[i].equals("..")) // faut vérifier si on est pas sur root
				{
					
					if (id != 1) {
						Dossier dossier = bdd.find(id);
						id = dossier.getDossierparent();

						String[] parts = dossier.getchemin().split("/");
						chemin = "";
						for (i = 0; i < parts.length - 2; i++) {
							chemin += parts[i] + "/";
						}
						if (chemin == "") {
							chemin = "/";
						} else {
							chemin += parts[i];
						}
					}

				}

			}

		} else {
			
			id = Dossiercourant_singleton.getInstance().id;
			chemin = Dossiercourant_singleton.getInstance().path;
			if(chemin=="/"){chemin="";}
			
			for (i = 0; i < names.length; i++) {
				
				if (!names[i].equals("") && !names[i].equals("..") && !names[i].equals(".")) {
					chemin += "/" + names[i];
					
					Dossier dossier = bdd.find_chemin(chemin);
					if (dossier != null) {
						id = dossier.getId();
					} else {
						break;
					}

				}
				if (names[i].equals("..")) // faut vérifier si on est pas sur root
				{
					
					if (id != 1) {
						Dossier dossier = bdd.find(id);
						id = dossier.getDossierparent();	
						String[] parts = dossier.getchemin().split("/");
						chemin = "";
						for (i = 0; i < parts.length - 2; i++) {
							chemin += parts[i] + "/";
						}
						if (chemin == "") {
							chemin = "/";
						} else {
							chemin += parts[i];
						}
					}

				}
			}

		}
		}
		
		if (i < names.length) {
			System.out.println("dossier inexistant");
		} else {
			if(chemin==""){chemin="/";}
			Dossiercourant_singleton.getInstance().path = chemin;
			Dossiercourant_singleton.getInstance().id = id;
			
		}
	}

}
