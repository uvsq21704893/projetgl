package uvsq.ProjetGL;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Fichier_Dao implements DAO<fichier> {

	Connection connection = Dossiercourant_singleton.getInstance().connection;

	@Override
	public fichier create(fichier obj) {
		try {
            boolean b=false;
			String query = "INSERT INTO `fichier` (nom ,taille, format,annotation,contenu) VALUES " + " (\""
					+ obj.getNom() + "\" ," + obj.getTaille() + " , \"" + obj.getFormat() + "\" , \""
					+ obj.getAnnotation() + "\" ,\"" + obj.getContenu() + "\");";

			Statement statement = connection.createStatement();
			statement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
			ResultSet results = statement.getGeneratedKeys();
			results.next();
			obj.setId(results.getInt(1));
			Dossier_Dao bdd = new Dossier_Dao();

			ArrayList<String> liste = obj.getChemin();
			for (int i = 0; i < liste.size(); i++) {
				String chemin= liste.get(i);
				Dossier dir=null;
				if (chemin.equals(".") && b==false){
					dir = bdd.find_chemin(Dossiercourant_singleton.getInstance().path);
					b=true;
				}
				if (!chemin.equals("."))
				{
					dir = bdd.find_chemin(liste.get(i)); // retrouver le id des chemins
				}
				
				if (dir !=null){
				query = "INSERT INTO `fich_dos` (idfichier ,iddossier) VALUES " + " (\"" + obj.getId() + "\" ," + dir.getId() + ");";
				statement.executeUpdate(query);
				}
			}
			statement.close();

			return obj;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public fichier find(int id) {
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM fichier where id=" + id + ";");

			fichier fichier = null;

			if(resultSet.next())
			{
			int taille = resultSet.getInt("taille");
			String nom = resultSet.getString("nom");
			String annotation = resultSet.getString("annotation");
			String format = resultSet.getString("format");
			String contenu = resultSet.getString("contenu");
			ArrayList<String> chemin = new ArrayList<String>();

			ResultSet resultSet2 = statement.executeQuery(
					"SELECT dossier.chemin FROM fich_dos,dossier where fich_dos.iddossier=dossier.id AND fich_dos.idfichier="
							+ id + ";");
			while (resultSet2.next()) {

				chemin.add(resultSet2.getString("dossier.chemin"));

			}
			fichier = new fichier.Builder(nom,format).setid(id).settaile(taille).setcontenu(contenu)
					.setchemin(chemin).setannotation(annotation).settaile(taille).build();
			resultSet2.close();
			}
			resultSet.close();
			statement.close();

			return fichier;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<fichier> find_nom(String nom) {
		try {
			Statement statement = connection.createStatement();
			Statement statement2 = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM fichier where nom= \"" + nom + " \";");

			fichier fichier = null;
			ArrayList<fichier> files = new ArrayList<fichier>();
			while (resultSet.next()) {

				int taille = resultSet.getInt("taille");
				int id = resultSet.getInt("id");
				String annotation = resultSet.getString("annotation");
				String format = resultSet.getString("format");
				String contenu = resultSet.getString("contenu");
				ArrayList<String> chemin = new ArrayList<String>();

				ResultSet resultSet2 = statement2.executeQuery(
						"SELECT dossier.chemin FROM fich_dos,dossier where fich_dos.iddossier=dossier.id AND fich_dos.idfichier="
								+ id + ";");
				while (resultSet2.next()) {

					chemin.add(resultSet2.getString("dossier.chemin"));
				}
				fichier = new fichier.Builder(nom,format).setid(id).settaile(taille).setcontenu(contenu).setchemin(chemin).setannotation(annotation).settaile(taille).build();
				files.add(fichier);

				resultSet2.close();

			}

			resultSet.close();
			statement.close();
			return files;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public fichier find_courant(String nom,int idroot) {
		try {
			Statement statement = connection.createStatement();
			Statement statement2 = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM fichier, fich_dos where fichier.nom= \"" + nom + " \" AND fich_dos.iddossier="+idroot+" ;");

			fichier fichier = null;
			
			while (resultSet.next()) {
				int taille = resultSet.getInt("taille");
				int id = resultSet.getInt("id");
				String annotation = resultSet.getString("annotation");
				String format = resultSet.getString("format");
				String contenu = resultSet.getString("contenu");
				ArrayList<String> chemin = new ArrayList<String>();

				ResultSet resultSet2 = statement2.executeQuery(
						"SELECT dossier.chemin FROM fich_dos,dossier where fich_dos.iddossier=dossier.id AND fich_dos.idfichier="
								+ id + ";");
				while (resultSet2.next()) {

					chemin.add(resultSet2.getString("dossier.chemin"));
				}
				fichier = new fichier.Builder(nom,format).setid(id).settaile(taille).setcontenu(contenu).setchemin(chemin).setannotation(annotation).settaile(taille).build();
				

				resultSet2.close();

			}

			resultSet.close();
			statement.close();
			return fichier;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<fichier> find_all(int idroot) {

		try {
			Statement statement = connection.createStatement(); //
			Statement statement2 = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("SELECT * FROM fichier,fich_dos where fich_dos.idfichier=fichier.id AND fich_dos.iddossier="+idroot+";");

			fichier fichier = null;
			ArrayList<fichier> files = new ArrayList<fichier>();
			while (resultSet.next()) {

				
				
				int taille = resultSet.getInt("fichier.taille");
				String nom = resultSet.getString("fichier.nom");
				int id = resultSet.getInt("fichier.id");
				String annotation = resultSet.getString("fichier.annotation");
				String format = resultSet.getString("fichier.format");
				String contenu = resultSet.getString("fichier.contenu");
				ArrayList<String> chemin = new ArrayList<String>();

				ResultSet resultSet2 = statement2.executeQuery(
						"SELECT dossier.chemin FROM fich_dos,dossier where fich_dos.iddossier=dossier.id AND fich_dos.idfichier="
								+ id + ";");
				while (resultSet2.next()) {

					chemin.add(resultSet2.getString("dossier.chemin"));
				}
				fichier = new fichier.Builder(nom,format).setid(id).settaile(taille).setcontenu(contenu).setchemin(chemin).setannotation(annotation).settaile(taille).build();
				files.add(fichier);

				resultSet2.close();
				
				

			}
			resultSet.close();
			statement.close();
			return files;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	//// chercher les fichiers par annotation
	public List<fichier> find_annot(String annot) {
		try {
			Statement statement = connection.createStatement();
			Statement statement2 = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM fichier where annotation= \"" + annot + " \";");

			fichier fichier = null;
			ArrayList<fichier> files = new ArrayList<fichier>();
			while (resultSet.next()) {

				int taille = resultSet.getInt("taille");
				String nom = resultSet.getString("nom");
				int id = resultSet.getInt("id");
				String format = resultSet.getString("format");
				String contenu = resultSet.getString("contenu");
				ArrayList<String> chemin = new ArrayList<String>();

				ResultSet resultSet2 = statement2.executeQuery(
						"SELECT dossier.chemin FROM fich_dos,dossier where fich_dos.iddossier=dossier.id AND fich_dos.idfichier="
								+ id + ";");
				while (resultSet2.next()) {

					chemin.add(resultSet2.getString("dossier.chemin"));
				}
				fichier = new fichier.Builder(nom,format).setid(id).settaile(taille).setcontenu(contenu).setchemin(chemin).setannotation(annot).settaile(taille).build();
				files.add(fichier);

				resultSet2.close();

			}
			resultSet.close();
			statement.close();
			return files;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void delete(int id) {
		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate("DELETE FROM fich_dos where fich_dos.idfichier=" + id + ";");
			statement.executeUpdate("DELETE FROM fichier where id=" + id + ";");
			statement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public fichier update(String oldname, String newname) {
		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate("UPDATE fichier set nom = \""+newname+"\" where nom=\"" + oldname + "\";");
			statement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<fichier> find_contenu(String cont) {

		try {
			Statement statement = connection.createStatement(); //
			Statement statement2 = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("SELECT * FROM fichier where fichier.contenu LIKE '%"+ cont+ "%';");

			fichier fichier = null;
			ArrayList<fichier> files = new ArrayList<fichier>();
			while (resultSet.next()) {

				
				int taille = resultSet.getInt("fichier.taille");
				String nom = resultSet.getString("fichier.nom");
				int id = resultSet.getInt("fichier.id");
				String annotation = resultSet.getString("fichier.annotation");
				String format = resultSet.getString("fichier.format");
				String contenu = resultSet.getString("fichier.contenu");
				ArrayList<String> chemin = new ArrayList<String>();

				ResultSet resultSet2 = statement2.executeQuery(
						"SELECT dossier.chemin FROM fich_dos,dossier where fich_dos.iddossier=dossier.id AND fich_dos.idfichier="
								+ id + ";");
				while (resultSet2.next()) {

					chemin.add(resultSet2.getString("dossier.chemin"));
				}
				fichier = new fichier.Builder(nom,format).setid(id).settaile(taille).setcontenu(contenu).setchemin(chemin).setannotation(annotation).settaile(taille).build();
				files.add(fichier);

				resultSet2.close();
				
				

			}
			resultSet.close();
			statement.close();
			return files;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

}
