package uvsq.ProjetGL;

import java.util.ArrayList;

public class fichier implements FileSystem {


	private int id ;
	private String nom;
	private int taille;
	private String annotation; 
	private String format; 
	private String contenu; 
	private ArrayList<String> chemin; 

	public int getId() {return id;}
	public void setId(int id) {this.id=id;}
	public String getNom() {return nom;}
	public int getTaille() {return taille;}
	public String getAnnotation() {return annotation;}
	public String getFormat() {return format;}
	public String getContenu() {return contenu;}
	public ArrayList<String> getChemin() {return chemin;}

	public static class Builder {
		private int id;
		private String nom;
		private int taille;
		private String annotation; 
		private String format; 
		private String contenu; 
		private ArrayList<String> chemin;
		public Builder(String nom, String format) {this.nom=nom;this.format=format;}
		
		public Builder setid (int id) {
			this.id=id;
			return this;
		}
		public Builder settaile (int taille) {
			this.taille=taille;
			return this;
		}
		public Builder setcontenu (String contenu) {
			this.contenu=contenu;
			return this;
		}
		public Builder setchemin (ArrayList<String> chemin) {
			this.chemin=chemin;
			return this;
		}
		public Builder setannotation(String annotation) {
			this.annotation=annotation;
			return this;
		}

		public fichier build() {
			return new fichier(this);
		}




	}
		
		private fichier(Builder builder) {
			id =builder.id;
			nom=builder.nom;
			taille=builder.taille;
			annotation=builder.annotation; 
			format=builder.format; 
			contenu=builder.contenu; 
			chemin=builder.chemin;
			}

	

	
	}

