package uvsq.ProjetGL;

import java.util.ArrayList;


public class Invokeur {

	
	private ArrayList<Commande> commandes= new ArrayList<Commande>(); 
	
	public void addNewCommand(Commande cmd)
	
	{  
		commandes.add(cmd);
	}
	
	
	public void invoquer()
	
	{ 	
		
	for (Commande cmd : commandes) {
        cmd.executer();
    }
	commandes.clear();
	}
}
