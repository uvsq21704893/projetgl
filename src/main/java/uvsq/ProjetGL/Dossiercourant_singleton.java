package uvsq.ProjetGL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Dossiercourant_singleton {

	
	private static Dossiercourant_singleton INSTANCE;
	public Connection connection;
	public String path;
	public int id;
	private Dossiercourant_singleton(String path, int id){
		
		try {
			this.path=path;
			this.id=id;
			Class.forName("com.mysql.jdbc.Driver");
			if (connection == null) {
				String url = "jdbc:mysql://localhost/projetgl";
				String user = "root";
				String password = "";
				this.connection = DriverManager.getConnection(url, user, password);
			}
		} catch (ClassNotFoundException e) {e.printStackTrace();} catch (SQLException e) {e.printStackTrace();}
	
	}
	
	public static Dossiercourant_singleton getInstance() {
		if (INSTANCE == null ) { 
		INSTANCE = new Dossiercourant_singleton("/", 1) ;
		}
		return INSTANCE;
		}
}
