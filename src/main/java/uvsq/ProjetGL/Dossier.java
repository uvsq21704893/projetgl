package uvsq.ProjetGL;

import java.util.ArrayList; 



public class Dossier  implements FileSystem  {
	

     private int id ;
	 private String nom; 
	 private int dossierparent;
	 String chemin; 
	// private Dossier parentdir;
	 private ArrayList<FileSystem>  files = new ArrayList<FileSystem>();
	 
	
	 public Dossier(String nom,int dossierparent,String chemin){this.nom=nom;this.dossierparent=dossierparent;this.chemin=chemin;}
	 public Dossier(int id,String nom,int dossierparent,String chemin){this.id=id;this.nom=nom;this.dossierparent=dossierparent;this.chemin=chemin;}
	
	 public int getId() {return id;}
	 public void setId(int id) {this.id=id;}
	 public String getNom() {return nom;}
	 public int getDossierparent() {return dossierparent;}
	 public String getchemin() {return chemin;}

	 
	 public void add(fichier f) { files.add(f);} 	 
	 public void remove(fichier f){ files.remove(f);}


}
