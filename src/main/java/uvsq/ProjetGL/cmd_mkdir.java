package uvsq.ProjetGL;

public class cmd_mkdir implements Commande {

	String nom;

	public cmd_mkdir(String nom) {
		this.nom = nom;
	}

	@Override
	public void executer() {
		if (!nom.equals("..") && !nom.equals(".")) {
			String[] names = nom.split("/");

			Dossier_Dao bdd = new Dossier_Dao();
			if (names.length > 0) {
				if (names[0].length() == 0) /// dans le cas ou on crée un
											/// dossier exemple :
				/// /facture/pdf
				{
					String chemin = "";
					int id = 1;
					int b = 0;
					for (int i = 1; i < names.length; i++) {
						if (names[i] != "") {
							chemin += "/" + names[i];

							Dossier dossier = bdd.find_chemin(chemin);
							if (dossier == null) {
								dossier = new Dossier(names[i], id, chemin);
								dossier = bdd.create(dossier);
								b = 1;
							}
							id = dossier.getId();
						}
					}
					if (b == 0) {
						System.out.println("dossier existant");
					}

				} else {
					int id = Dossiercourant_singleton.getInstance().id;
					int b = 0;
					String chemin = Dossiercourant_singleton.getInstance().path;
					if (chemin == "/") {
						chemin = "";
					}
					for (int i = 0; i < names.length; i++) {
						if (names[i] != "") {
							chemin += "/" + names[i];

							Dossier dossier = bdd.find_chemin(chemin);
							if (dossier == null) {
								dossier = new Dossier(names[i], id, chemin);
								dossier = bdd.create(dossier);
								b = 1;
							}

							id = dossier.getId();
						}
					}
					if (b == 0) {
						System.out.println("dossier existant");
					}
				}

			}

		}

		else {
			System.out.println("erreur de syntaxe");
		}
	}
}
