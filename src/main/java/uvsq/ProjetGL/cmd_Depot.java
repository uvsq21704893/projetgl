package uvsq.ProjetGL;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class cmd_Depot implements Commande {

	fichier fichier;
	ArrayList<String> chemin=new ArrayList<String>();


	public void executer() {

		/// création du dossier inbox
		(new File("./INBOX")).mkdirs();
		String str;
		JFileChooser chooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG & GIF Images", "jpg", "gif", "pdf", "txt",
				"mp4", "png", "docx");
		chooser.setFileFilter(filter);
		chooser.setCurrentDirectory(new java.io.File("."));
		chooser.setDialogTitle("choosertitle");
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setAcceptAllFileFilterUsed(false);
		chooser.setMultiSelectionEnabled(true);
		File[] f;
		if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			
			f = chooser.getSelectedFiles();
			for (int i = 0; i < chooser.getSelectedFiles().length; i++) {
				String contenu = null;
				File source = f[i];
				String name = source.getName();
				int taille = (int) source.length();
				String[] names = name.split("\\.");
				name = names[0];
				String[] noms = name.split(" ");
				String name2="";
				for (int z=0; z<noms.length;z++)
				{
					name2+=noms[z];
					if (z!= noms.length-1){name2+= "_";}
				}
				name=name2;
				String format = names[1];
				String annotation = null;
				Reconnaissance reconnaissance=null;
				if (format.toLowerCase().equals("docx")) {
					annotation = "text";
					reconnaissance = new ReconnaissanceDOCX();
					
				}
				if (format.toLowerCase().equals("txt")) {
					annotation = "text";
					reconnaissance = new ReconnaissanceTXT();
					
				}
				if (format.toLowerCase().equals("pdf") ) {
					annotation = "text";
					reconnaissance = new ReconnaissancePDF();
					
				}
				if (format.toLowerCase().equals("jpg") || format.toLowerCase().equals("png")|| format.toLowerCase().equals("gif")) {
					annotation = "image";
					
					reconnaissance = new ReconnaissanceOCR();
					
				}
				if (format.toLowerCase().equals("mp4")) {
					annotation = "video";
				}
				
				
				if(reconnaissance != null)
				{
					reconnaissance .extraireText(source);
					contenu = reconnaissance .contenu;
				}

				//////////// récupération du contenu
							
				String motcle="";
				
				if (contenu !=null)
				{	
				try {

					List<Keyword> keywordsList= extraction.guessFromString(contenu);
					int j; j=0; 
					
					
					while (j<10 && j<keywordsList.size()) {

						Keyword word = keywordsList.get(j);
						
						List<String> list = new ArrayList<String>();
						
						list.addAll((Collection<? extends String>) word.getTerms());
						
						for(int k =0 ; k<list.size(); k++)
						
						{

					if (list.get(k).length()>5) {
					      motcle+="/";
							 motcle+= list.get(k);
                        }

						}
						
						j++;
					}
					
						} catch (IOException e) {
					
					e.printStackTrace();
				}
				}
				
				///// reccupération du chemin
				Scanner sc = new Scanner(System.in);
				
				do {
					System.out.println("préciser le ou les dossiers (mettre '.' pour le dossier courant) :");
					str = sc.nextLine();
					chemin.add(str);
					if (!str.equals(".") && !str.equals("..")){
						
					
						if (!str.contains("."))
						{
						
							Commande cmd = new cmd_mkdir(str);
							cmd.executer();
						}
						else 
						{
							System.out.println("erreur de syntaxe");
						}
					}
					System.out.println("voulez vous donner un autre dossier o/n ?");
					str = sc.nextLine();
				} while (str.equals("o"));

				
				
				fichier = new fichier.Builder(name, format).settaile(taille).setannotation(annotation)
						.setcontenu(motcle).setchemin(chemin).build();
				Fichier_Dao fichDao = new Fichier_Dao();
				fichDao.create(fichier);

			}

		} else {
			System.out.println("No Selection ");
		}

	}
	
	


}