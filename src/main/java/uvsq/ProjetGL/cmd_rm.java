package uvsq.ProjetGL;

public class cmd_rm implements Commande {
	    
	String nom;
		public cmd_rm(String nom){this.nom=nom;}
		
		public void executer() {
			
			Fichier_Dao bdd = new Fichier_Dao();
			
			fichier f = bdd.find_courant(nom,Dossiercourant_singleton.getInstance().id);
			
			if(f == null)
			{
				System.out.println("fichier inexistant dans ce repertoir");
			}
			else
			{
				bdd.delete(f.getId());
				System.out.println("fichier supprimé");
			}
			
		}
}
