package uvsq.ProjetGL;

import java.io.BufferedWriter; 
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import com.asprise.ocr.Ocr;

public class ReconnaissanceOCR  extends Reconnaissance {

	
	
	
	public File extraireText(File file) {
		
	try{
							Ocr.setUp(); // one time setup
							Ocr ocr = new Ocr(); // create a new OCR engine
							ocr.startEngine("fra", Ocr.SPEED_FASTEST); // french
							contenu = ocr.recognize(new File[] {new File(file.getAbsolutePath())}, 
									Ocr.RECOGNIZE_TYPE_ALL, Ocr.OUTPUT_FORMAT_PLAINTEXT);
	
		} 
		catch (Exception e){
			e.printStackTrace();
		}
		
		return null; 
	}
}
