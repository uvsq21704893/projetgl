package uvsq.ProjetGL;


import java.util.Scanner;

public class recepteur {

	public void recepteur() {

		Scanner sc = new Scanner(System.in);
		Invokeur inv =new Invokeur();
		Commande cmd;
		while (true)
		{

				System.out.print(Dossiercourant_singleton.getInstance().path + " $ ");
				String str = sc.nextLine();
				String[] commande = str.split(" ");
			if (commande[0].equals("depot"))
			{
				cmd = new cmd_Depot(); 
				inv.addNewCommand(cmd);
				inv.invoquer();
			
			}
			if (commande[0].equals("cd"))
			{
				cmd = new cmd_cd(commande[1]);
				inv.addNewCommand(cmd);
				inv.invoquer();
			
			}
			if (commande[0].equals("mkdir"))
			{
				cmd = new cmd_mkdir(commande[1]);
				inv.addNewCommand(cmd);
				inv.invoquer();
			
			}
			if (commande[0].equals("annot"))
			{
			cmd = new cmd_Annotation(commande[1]);
			inv.addNewCommand(cmd);
			inv.invoquer();
				}
				if (commande[0].equals("nom"))
				{
					cmd = new cmd_ParNom(commande[1]);
					inv.addNewCommand(cmd);
					inv.invoquer();
					
				}
				if (commande[0].equals("keyword"))
				{
					cmd = new cmd_FullText(commande[1]);
					inv.addNewCommand(cmd);
					inv.invoquer();
					
				}
				if (commande[0].equals("ls"))
				{
					cmd = new cmd_Hierarchie();
					inv.addNewCommand(cmd);
					inv.invoquer();
				}
				if (commande[0].equals("rm"))
				{
					cmd = new cmd_rm(commande[1]);
					inv.addNewCommand(cmd);
					inv.invoquer();
				}
				if (commande[0].equals("mv"))
				{
					cmd = new cmd_mv(commande[1], commande[2]);
					inv.addNewCommand(cmd);
					inv.invoquer();
					
				}
				if (str.equals("exit"))
				{
					cmd = new cmd_exit();
					inv.addNewCommand(cmd);
					inv.invoquer();
				}



		}


	}

}
