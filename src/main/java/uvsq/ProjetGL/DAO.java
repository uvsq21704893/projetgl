package uvsq.ProjetGL;

import java.util.List;

public interface DAO<T> {
	public abstract T create (T obj) ;
	public abstract T find ( int id ) ;
	public abstract List<T> find_all ( int idroot ) ;
	public abstract void delete ( int id ) ;
	public abstract T update (String oldname, String newname ) ;
}
