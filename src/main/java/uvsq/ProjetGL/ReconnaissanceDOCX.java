package uvsq.ProjetGL;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;

public class ReconnaissanceDOCX extends Reconnaissance {
	 
	
	public File extraireText(File file) {
		
		File output = null;
		
	    try {
	    	BufferedWriter wr = null;	        
	        FileInputStream fis = new FileInputStream(file.getAbsolutePath());
	        XWPFDocument document = new XWPFDocument(fis);
	        output = new File("TextOfFileProcessed.txt");
	        List<XWPFParagraph> paragraphs = document.getParagraphs();

	        for (XWPFParagraph para : paragraphs) {
	            
                 contenu+= " " + para.getText();
	            fis.close();
	        }
	        wr = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(output))); //pattern decorator
			
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return output; 
	}
}
