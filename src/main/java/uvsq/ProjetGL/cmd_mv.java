package uvsq.ProjetGL;

import java.util.List;

public class cmd_mv implements Commande  {

	
	String oldname;
	String newname;
	public cmd_mv(String oldname,String newname){this.oldname=oldname; this.newname=newname;}
	
	public void executer() {
		
		Fichier_Dao bdd = new Fichier_Dao();
		
		List<fichier> f = bdd.find_nom(oldname);
		
		if(f.size() == 0)
		{
			System.out.println("fichier inexistant");
		}
		else
		{
			bdd.update(oldname, newname);
			System.out.println("fichier modifié");
		}
		
	}
}
