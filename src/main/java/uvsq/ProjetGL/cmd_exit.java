package uvsq.ProjetGL;

public class cmd_exit implements Commande {

	
	
	@Override
	public void executer() {
		try {
			if (Dossiercourant_singleton.getInstance().connection != null) {
				Dossiercourant_singleton.getInstance().connection.close();
			}
		} catch (Exception e) {
			// do nothing
		}
		
		System.exit(0);
		
	}

}
