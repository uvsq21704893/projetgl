package uvsq.ProjetGL;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Test_cmd_Hierarchie {

	
	cmd_Hierarchie cmd;
	Fichier_Dao test;
	@Before
	public void setUp() throws Exception {
		Db_init.dbinit();
		test = new Fichier_Dao();
		
		
		int taille = 200;
   		String nom = "test";
   		String annotation = "text";
   		String format = "pdf";
   		String contenu =  "attestation";
   		ArrayList<String> chemin = new ArrayList<String>();
   		chemin.add("/"); chemin.add("/facture");
		fichier obj = new fichier.Builder(nom,format).settaile(taille).setcontenu(contenu).setchemin(chemin).setannotation(annotation).settaile(taille).build();
	
		
		cmd_mkdir make = new cmd_mkdir("facture");
	
		make.executer();
		cmd_cd cd = new cmd_cd("facture");
		cd.executer();
		cd = new cmd_cd("..");
		cd.executer();
		test.create(obj);
		
	}
	
	@After
	public void tearDown() throws Exception {
		Db_init.dbinit();
	}
	
	@Test
	public void test() {
		cmd = new cmd_Hierarchie();
		cmd.executer();
		
		
	}
}
