package uvsq.ProjetGL;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;

public class Test_dossier {
	
	Dossier test;
	@Before
	public void setUp() throws Exception {
		test = new Dossier("test",1,"/test");
	}
	
	@Test
	public void test() throws Exception {
		
		Assert.assertNotNull(test);
		Assert.assertEquals(test.getNom(), "test");
		Assert.assertEquals(test.getDossierparent(), 1);
		Assert.assertEquals(test.getchemin(), "/test");
	}
}
