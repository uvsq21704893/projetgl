package uvsq.ProjetGL;


import org.junit.Test;

import junit.framework.Assert;

public class Dossiercourant_singletonTest {

	@Test
	public void test() {
		Assert.assertNotNull(Dossiercourant_singleton.getInstance());
		Assert.assertEquals(Dossiercourant_singleton.getInstance().path, "/");
		Assert.assertEquals(Dossiercourant_singleton.getInstance().id, 1);
	}

}
