package uvsq.ProjetGL;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class cmd_cdTest {

	Dossier_Dao test;
	cmd_cd cmdtest;

	@Before
	public void setUp() throws Exception {
		test = new Dossier_Dao();
		Db_init.dbinit();
	}

	@After
	public void tearDown() throws Exception {
		Db_init.dbinit();
	}

	@Test
	public void test() {
		cmd_mkdir makedir = new cmd_mkdir("facture");
		makedir.executer();
		cmdtest = new cmd_cd("facture");
		cmdtest.executer();
		assertEquals(Dossiercourant_singleton.getInstance().id, 2);
		assertEquals(Dossiercourant_singleton.getInstance().path, "/facture");
		cmd_mkdir makedir2 = new cmd_mkdir("pdf");
		makedir2.executer();

		cmdtest = new cmd_cd("pdf");
		cmdtest.executer();
		assertEquals(Dossiercourant_singleton.getInstance().id, 3);
		assertEquals(Dossiercourant_singleton.getInstance().path, "/facture/pdf");
		cmdtest = new cmd_cd(".");
		cmdtest.executer();
		assertEquals(Dossiercourant_singleton.getInstance().id, 3);
		assertEquals(Dossiercourant_singleton.getInstance().path, "/facture/pdf");

		cmdtest = new cmd_cd("..");
		cmdtest.executer();
		assertEquals(Dossiercourant_singleton.getInstance().id, 2);
		assertEquals(Dossiercourant_singleton.getInstance().path, "/facture");
		cmdtest.executer();
		assertEquals(Dossiercourant_singleton.getInstance().id, 1);
		assertEquals(Dossiercourant_singleton.getInstance().path, "/");
		cmdtest.executer();
		assertEquals(Dossiercourant_singleton.getInstance().id, 1);
		assertEquals(Dossiercourant_singleton.getInstance().path, "/");

		cmdtest = new cmd_cd(".");
		cmdtest.executer();

		assertEquals(Dossiercourant_singleton.getInstance().id, 1);
		assertEquals(Dossiercourant_singleton.getInstance().path, "/");
		cmdtest = new cmd_cd("/");
		cmdtest.executer();
		
		assertEquals(Dossiercourant_singleton.getInstance().id,1);
		
			assertEquals(Dossiercourant_singleton.getInstance().path, "/");
		cmdtest = new cmd_cd("image");
		cmdtest.executer();

		assertEquals(Dossiercourant_singleton.getInstance().id, 1);
		assertEquals(Dossiercourant_singleton.getInstance().path, "/");

	}

}
