package uvsq.ProjetGL;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;

public class Fichier_DaoTest {

	
	Fichier_Dao test;
	@Before
	public void setUp() throws Exception {
		test = new Fichier_Dao();
		Db_init.dbinit();
	}
	
	@After
	public void tearDown() throws Exception {
		Db_init.dbinit();
	}
	
	@Test
	public void testcreate() throws Exception {
		
   		int taille = 200;
   		String nom = "test";
   		String annotation = "text";
   		String format = "pdf";
   		String contenu =  "attestation";
   		ArrayList<String> chemin = new ArrayList<>();
   		chemin.add("/");
		fichier obj = new fichier.Builder(nom,format).settaile(taille).setcontenu(contenu).setchemin(chemin).setannotation(annotation).settaile(taille).build();
		
		Assert.assertNotNull(test.create(obj));
	    Assert.assertNotNull(obj.getId());
	}
	
	@Test
	public void testfind() throws Exception {
		testcreate();
		fichier obj = test.find(1);	
		Assert.assertNotNull(obj);
		Assert.assertNotNull(obj.getId());
		Assert.assertEquals(obj.getChemin().get(0), "/");
	}
   @Test
	public void test_nom() throws Exception {
		testcreate();
		List <fichier> obj = test.find_nom("test");	
		Assert.assertNotNull(obj);

		assertEquals (obj.get(0).getId(),1);
		
	}
}
