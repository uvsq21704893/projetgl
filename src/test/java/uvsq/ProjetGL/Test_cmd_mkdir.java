package uvsq.ProjetGL;

import static org.junit.Assert.*;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Test_cmd_mkdir {

	@Before
	public void setUp() throws Exception {
		Db_init.dbinit();
	}
	
	@After
	public void tearDown() throws Exception {
		Db_init.dbinit();
	}
	@Test
	public void test_cmd_mkdir() {
		
		
		cmd_mkdir c = new cmd_mkdir("facture");
	    c.executer();
	    
	    Dossier_Dao bdd= new Dossier_Dao();
	    Dossier ds = bdd.find_nom("facture",Dossiercourant_singleton.getInstance().id);
	    String name = ds.getNom();
	    assertEquals (ds.getNom(),name);
	    assertEquals (ds.getId(),2);
	    assertEquals (ds.getchemin(),"/facture");
	    c.executer(); /// on ne peut pas créer deux fois le meme dossier et il doit nous afficher erreur de création du dossier 
		
	}

}
