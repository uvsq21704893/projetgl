package uvsq.ProjetGL;

import java.awt.List;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;

public class Test_file {

	
	fichier test;
	private String[] chemins;
	@Before
	public void setUp() throws Exception {
		int taille = 200;
   		String nom = "test";
   		String annotation = "text";
   		String format = "pdf";
   		String contenu =  "attestation";
   		ArrayList<String> chemin = new ArrayList<String>(); 
   		chemin.add("/"); 
   		chemin.add("test");
   		
   		test = new fichier.Builder(nom,format).settaile(taille).setcontenu(contenu).setchemin(chemin).setannotation(annotation).settaile(taille).build();
	}
	
	@Test
	public void test() throws Exception {
		
		Assert.assertNotNull(test);
		
		Assert.assertEquals(test.getNom(),"test");
		Assert.assertEquals(test.getAnnotation(), "text");
		Assert.assertEquals(test.getFormat(),"pdf");
	    Assert.assertEquals(test.getChemin().size(), 2);
		Assert.assertEquals(test.getContenu(),"attestation");
		Assert.assertEquals(test.getTaille(), 200);
	}
}
