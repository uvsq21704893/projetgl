package uvsq.ProjetGL;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class cmd_mvTest {
	
	
	cmd_mv cmd;
	Fichier_Dao test;
	@Before
	public void setUp() throws Exception {
		Db_init.dbinit();
		test = new Fichier_Dao();
		
		
		int taille = 200;
   		String nom = "test";
   		String annotation = "text";
   		String format = "pdf";
   		String contenu =  "attestation";
   		ArrayList<String> chemin = new ArrayList<String>();
   		chemin.add("/"); chemin.add("/facture");
		fichier obj = new fichier.Builder(nom,format).settaile(taille).setcontenu(contenu).setchemin(chemin).setannotation(annotation).settaile(taille).build();
	
		
		cmd_mkdir make = new cmd_mkdir("facture");
	
		make.executer();
		cmd_cd cd = new cmd_cd("facture");
		cd.executer();
		test.create(obj);
		
		
	}
	
	@After
	public void tearDown() throws Exception {
		Db_init.dbinit();
	}
	@Test
	public void test() {
		cmd = new cmd_mv("test","EDF");
		cmd.executer();
		cmd_ParNom cmd2 = new cmd_ParNom("EDF");
		cmd2.executer();
	}

}
