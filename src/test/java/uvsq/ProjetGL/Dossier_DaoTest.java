package uvsq.ProjetGL;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import junit.framework.Assert;
import junit.framework.TestCase;

public class Dossier_DaoTest extends TestCase {

	Dossier_Dao test;
	@Before
	public void setUp() throws Exception {
		test = new Dossier_Dao();
		Db_init.dbinit();
	}
	@After
	public void tearDown() throws Exception {
		Db_init.dbinit();
	}
	  
		
		
		@Test
		public void testcreate() throws Exception {
			
			Dossier obj = new Dossier("test",1,"/test");
					
			Assert.assertNotNull(test.create(obj));
		    Assert.assertNotNull(obj.getId());
		}
		@Test
		public void testfind() throws Exception {
			
			Dossier obj = test.find(1);
			
			
			Assert.assertNotNull(obj);
			Assert.assertNotNull(obj.getId());
			System.out.println(obj.getId());
		}
		
		@Test
		public void testfind_nom() throws Exception {
			testcreate();
			Dossier obj = test.find_nom("test",1);
			
			
			Assert.assertNotNull(obj);
			assertEquals (obj.getId(),2);
			assertEquals (obj.getNom(), "test");
			
		}
}
