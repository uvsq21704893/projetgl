![Alt text](https://bytebucket.org/uvsq21704893/projetgl/raw/85ce0402bc5c7ae0bb62a26d86f8a747502550b6/schema.png?token=5ea75f61a2e9b49c07eb2f6923580ed206a22ec6)

**Les patterns utilis�s dans notre application** :
===============================================

##1.pattern **_Builder_**

##2.pattern **_Composite_**

##3.pattern **_Commande_**

##4.pattern **_Singleton_**

##5.pattern **_DAO_** (Base de donn�e)

**Utilisation des patterns**:
-----------------------------

##1.pattern **_Buider_** : est utilis� pour l'instanciation des objets de la classe fichier.

##2.pattern **_Composite_** : est utilis� pour la hi�rarchie de fichier et de dossier.

##3.pattern **_Commande_** : 

>##commande **_mkdir_** : pour la cr�ation de nouveau dossier.

>##commande **_remove _**(rm): pour la suppression des fichiers.

>##commande **_Cd_** : pour se bamader dans la hi�rarchie.
   
>##commande **_exit_** : pour sortir de la console.
   
>##commande **_mv_** (rename): pour renommer un fichier.

>##commande **_Keyword_** (recherche par mot cl�) : pour la recherche par mots cl� dans les fichiers.
   
>##commande **_depot_** : pour deposer un ou plusieurs fichiers.

>##commande **_keyword_** : lister les fichiers qui contient les mots cl� propos� par l'utilisateur.

>##commande **_Ls_** : lister les fichiers et les dossiers dans le dossier courant.

>##commande **_nom_** : recherche les fichiers ayant un nom sp�cifique.

>##commande **_annot_** : recherche un fichier par rapport � son annoatation (gestion de 3 types d'annoations : <text> por les pdf, txt et docx
<image> pour les jpg, png et gif <video> pour les fichiers mp4).


##4.pattern **_Singleton_** : pour la connexion � la base de donn�es et la sauvegarde du dossier courant (son id et son chemin : path).

##5.pattern **_DAO_** : est utilis� pour la persistance des objets (interrogation de la base de donn�es).

>##**db_init** : la classe pour l'initialisation de nos tables (ce script est execut� une seule fois). 

>##**File_Dao** ,**dossier_Dao **: pour la persistance des objets File et dossier respectivement.

**Annotation utilis�es**
------------------------
## On a utilis� trois annotations : TEXT, IMAGE, VIDEO.

**Reconnaissance**
------------------

## Utilisation 4 classes : reconnaissancePDF, reconnaissanceOCR, reconnaissanceTXT, reconnaissanceDOCX.

**Extraction des mots cl�**
-----------------------

## se fait avec lucene qui se base sur la fr�quence d'apparition des mots dans le fichier puis renvoitune liste de <Keyword> pour une recherche ult�rieure.
## d'un autre c�t� on garde dans la base de donn�es que les mots cl�s qui se r�p�tes le plus dans le fichier ett non tous le contenu du fichier ou tous les mots cl�

**Pom.xml**
-----------
## Des d�pendances ont �t� ajout� : Mysql, PFDBox, OCR, POI-ooxml, lucene

**INBOX**
---------

## choix de fichiers se fait avec choser et contient tous les fichiers ayant comme extention (PDF, TXT, DOCX, PNG, JPG, GIF,MP4).

**Base de donn�es**
---------

## une table fichier : id, nom, format, taille, annotation, contenu.
## une table dossier : id, nom, chemin, idroot. (il s'agi du chemin absolu du dossier)
## une table fich_dos : id_fichier, id_dossier (car un dossier peut avoir plusieurs fichier et un fichier peut appartenir � plusieurs dossier ceci est sp�cifi� par l'utilisateur au mment du depot)


**explication du SHELL**
---------

## exemple des commandes :  
- $ cd .. , $ cd . , $ cd /test/pdf, $ cd pdf  (chemin absolu ou relatif)
- $ mkdir test , mkdir /test/pdf (chemin absolu ou relatif)
- $ ls  (tout court)
- $ depot (tout court) 
- $ rm nom_file  (sans mettre d'extention : edf.pdf -> mettre edf)
- $ mv edf2017 edf 2018 (mv ancien_nom nouveau_nom  sans espace)
- $ nom edf (recherche par nom sans mettre l'extention) 
- $ annot text,  $ annot image, $ annot video (recherche par annotation)
- $ keyword attestation, $ keyword informatique (recherche par mots cl� -> ici on fait une recherche par un seul mot cl�) 
 


   